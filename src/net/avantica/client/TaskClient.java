package net.avantica.client;

import net.avantica.taskManager.TaskEntity;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class TaskClient {
	public static void main(String[] args) {

        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client client = Client.create(clientConfig);

        TaskEntity tareaNueva = new TaskEntity();
        tareaNueva.setNombre("Repasar conceptos");
        tareaNueva.setAsignado("Bryan");
        tareaNueva.setEstado((short)2);
        
        String postURL = "http://localhost:8080/TaskManagerREST001/resources/Tasks";
        
        WebResource webResourcePost = client.resource(postURL);
        ClientResponse response = webResourcePost.type("application/json").post(ClientResponse.class, tareaNueva);
        TaskEntity responseEntity = response.getEntity(TaskEntity.class);

        System.out.println(responseEntity.toString());
    }
}
