package net.avantica.taskManager;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.avantica.taskManager.DAO.TaskDAO;

@Path("Tasks")
public class TaskResource {
    private static final TaskDAO dao = new TaskDAO();
    
    public TaskResource(){}
    
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getTaskList(){
		List<TaskEntity> listaTarea = new ArrayList<TaskEntity>();
		
		dao.openCurrentSession();
		listaTarea = dao.getTasks();
		dao.closeCurrentSession();
		
		return Response.status(200).entity(listaTarea).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_HTML})
	@Path("/{taskId}")
	public Response getTask(@PathParam("taskId")Long taskId){
		TaskEntity tarea = new TaskEntity();
		
		dao.openCurrentSession();
		tarea = dao.getTaskByID(taskId);
		dao.closeCurrentSession();
		
		if (null != tarea) {
			return Response.status(200).entity(tarea)
					.type(MediaType.APPLICATION_JSON).build();
		}
		else{
			return Response.status(404).type(MediaType.TEXT_HTML).build();
		}
		
	}
	
	@POST
	@Consumes("application/json")
	public Response createTask(TaskEntity nuevaTarea){
		dao.openCurrentSessionwithTransaction();
		dao.createTask(nuevaTarea);
		dao.closeCurrentSessionwithTransaction();
		
		return Response.status(204).build();
	}
	
	@DELETE
	@Path("/{taskId}")
	public Response deleteTask(@PathParam("taskId")Long taskId){
		dao.openCurrentSessionwithTransaction();
		TaskEntity tarea = dao.getTaskByID(taskId);
		dao.deleteTask(tarea);
		dao.closeCurrentSessionwithTransaction();
		
		return Response.status(204).build();
	}
	
	@PUT
	@Consumes("application/json")
	public Response updateTask(TaskEntity tarea){
		dao.openCurrentSessionwithTransaction();
		dao.updateTask(tarea);
		dao.closeCurrentSessionwithTransaction();
		
		return Response.status(204).build();
	}
}
