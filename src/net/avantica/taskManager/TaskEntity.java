package net.avantica.taskManager;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Entity;

@XmlRootElement
@Entity
@Table(name="tareas")
public class TaskEntity {
	@Id
	@GeneratedValue
	@Column(name="idTarea")
	private Long idTarea;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="asignado")
	private String asignado;
	
	@Column(name="estado")
	private Short estado;
	
	public TaskEntity(){}
	
	public TaskEntity(
			Long idTarea, String nombre, 
			String asignado, Short estado
			){
		this.idTarea = idTarea;
		this.nombre = nombre;
		this.asignado = asignado;
		this.estado = estado;
	}
	
	public Long getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(Long idTarea) {
		this.idTarea = idTarea;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getAsignado() {
		return asignado;
	}
	public void setAsignado(String asignado) {
		this.asignado = asignado;
	}
	
	public Short getEstado() {
		return estado;
	}
	public void setEstado(Short estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString(){
		StringBuilder respuesta = new StringBuilder();
		respuesta.append("Id: " + this.idTarea + '\n');
		respuesta.append("Tarea: " + this.nombre + '\n');
		respuesta.append("Responsable: " + this.asignado + '\n');
		respuesta.append("Estado: " + this.estado + '\n');

        return respuesta.toString();
	}
}
