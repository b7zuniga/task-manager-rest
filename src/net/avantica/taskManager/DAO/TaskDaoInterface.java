package net.avantica.taskManager.DAO;

import java.util.List;

import net.avantica.taskManager.TaskEntity;

public interface TaskDaoInterface {
	
	public void createTask(TaskEntity tareaNueva);
	
	public List<TaskEntity> getTasks();

	public TaskEntity getTaskByID(Long taskId);
	
	public void updateTask(TaskEntity tareaActualizada);
	
	public void deleteTask(TaskEntity tarea);
}
