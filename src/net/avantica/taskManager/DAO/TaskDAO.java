package net.avantica.taskManager.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import net.avantica.taskManager.TaskEntity;

public class TaskDAO implements TaskDaoInterface{
	
	private Session currentSession;
	private Transaction currentTransaction;
	
	private static final String SQL_LIST_ORDER_BY_ID =
	        "FROM tareas ORDER BY idTarea";
	
	public TaskDAO(){
		
	}
	
	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}
	
	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}
	
	public void closeCurrentSession() {
		currentSession.close();
	}
	
	
	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}
	
	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new 
				StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory =
				configuration.buildSessionFactory(builder.build());
		
		return sessionFactory;
	}
	
	public Session getCurrentSession() {
		return currentSession;
	}
	
	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}
	
	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}
	
	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}
	
	@Override
	public void createTask(TaskEntity tareaNueva) {
		getCurrentSession().save(tareaNueva);
	}

	@Override
	public List<TaskEntity> getTasks() {
		@SuppressWarnings("unchecked")
		List<TaskEntity> listaTareas = 
				(List<TaskEntity>)getCurrentSession()
				.createQuery("Select t from TaskEntity t").list();
		return listaTareas;
	}

	@Override
	public TaskEntity getTaskByID(Long taskId) {
		TaskEntity tarea = (TaskEntity)getCurrentSession()
							.get(TaskEntity.class, taskId);
			return tarea;
	}

	@Override
	public void updateTask(TaskEntity tareaActualizada) {
		getCurrentSession().update(tareaActualizada);
	}

	@Override
	public void deleteTask(TaskEntity tarea) {
		getCurrentSession().delete(tarea);
	}
	
}
